package ru.tsc.vinokurov.tm.service;

import ru.tsc.vinokurov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;


public class LoggerService implements ILoggerService {

    public static final String CONFIG_FILE = "/logger.properties";

    public static final String COMMANDS = "COMMANDS";

    public static final String COMMANDS_FILE = "./commands.xml";

    public static final String ERRORS = "ERRORS";

    public static final String ERROR_FILE = "./errors.xml";

    public static final String MESSAGES = "MESSAGES";

    public static final String MESSAGES_FILE = "./messages.xml";

    private final LogManager manager = LogManager.getLogManager();

    private final Logger root = Logger.getLogger("");

    private final Logger commands = Logger.getLogger(COMMANDS);

    private final Logger errors = Logger.getLogger(ERRORS);

    private final Logger messages = Logger.getLogger(MESSAGES);

    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERROR_FILE, true);
        registry(messages, MESSAGES_FILE, true);
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord logRecord) {
                return logRecord.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(final Logger logger, final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }
    @Override
    public void info(String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void command(String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void debug(String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

}
