package ru.tsc.vinokurov.tm.api.repository;

import ru.tsc.vinokurov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    boolean existsByLogin(String login);

    boolean existsByEmail(String email);

}
