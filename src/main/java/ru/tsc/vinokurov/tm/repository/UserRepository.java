package ru.tsc.vinokurov.tm.repository;

import ru.tsc.vinokurov.tm.api.repository.IUserRepository;
import ru.tsc.vinokurov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        return users.stream().filter(uzer -> id.equals(uzer.getId()))
                .findAny().orElse(null);
    }

    @Override
    public User findByLogin(final String login) {
        return users.stream().filter(uzer -> login.equals(uzer.getLogin()))
                .findAny().orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return users.stream().filter(uzer -> email.equals(uzer.getEmail()))
                .findAny().orElse(null);
    }

    @Override
    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public boolean existsByLogin(final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean existsByEmail(final String email) {
        return findByEmail(email) != null;
    }

}
