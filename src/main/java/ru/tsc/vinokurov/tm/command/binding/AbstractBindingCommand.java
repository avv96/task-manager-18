package ru.tsc.vinokurov.tm.command.binding;

import ru.tsc.vinokurov.tm.api.service.IProjectTaskService;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

public abstract class AbstractBindingCommand extends AbstractCommand {

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

}
