package ru.tsc.vinokurov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.repository.IUserRepository;
import ru.tsc.vinokurov.tm.api.service.IUserService;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.exception.entity.UserExistsByEmailException;
import ru.tsc.vinokurov.tm.exception.entity.UserExistsByLoginException;
import ru.tsc.vinokurov.tm.exception.field.EmailEmptyException;
import ru.tsc.vinokurov.tm.exception.field.PasswordEmptyException;
import ru.tsc.vinokurov.tm.exception.field.RoleEmptyException;
import ru.tsc.vinokurov.tm.exception.field.UserIdEmptyException;
import ru.tsc.vinokurov.tm.model.User;
import ru.tsc.vinokurov.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (StringUtils.isEmpty(id)) throw new UserIdEmptyException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new EmailEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) {
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.remove(user);
    }

    @Override
    public User removeById(final String id) {
        if (StringUtils.isEmpty(id)) throw new UserIdEmptyException();
        final User user = findById(id);
        if (user == null) return null;
        return userRepository.remove(user);
    }

    @Override
    public User removeByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new EmailEmptyException();
        final User user = findByLogin(login);
        if (user == null) return null;
        return userRepository.remove(user);
    }

    @Override
    public User create(final String login, final String password) {
        if (StringUtils.isEmpty(login)) throw new EmailEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        if (userRepository.existsByLogin(login)) throw new UserExistsByLoginException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt((password)));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        if (userRepository.existsByEmail(email)) throw new UserExistsByEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email, final Role role) {
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password, email);
        if (user == null) return null;
        user.setRole(role);
        return userRepository.add(user);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        final User user = findById(userId);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            final String userId, final String firstName,
            final String lastName, final String middleName
    ) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final User user = findById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
